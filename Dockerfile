FROM node:alpine as builder
WORKDIR /app
COPY package*.json ./
COPY tsconfig*.json ./
COPY ./src ./src
RUN npm install && npm run build

FROM node:alpine
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY --from=builder /app/build /app
EXPOSE 80
CMD npm run migration:prd:run && npm run start:prd