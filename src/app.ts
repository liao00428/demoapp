import dotenv from 'dotenv';
dotenv.config();
import express from 'express';
import cors from 'cors';
import { dataSource } from './dataSource';
import router from './routes/indexRoute';

dataSource.initialize()
    .then(() => {
        console.log("Data Source has been initialized!")
    })
    .catch((err) => {
        console.error("Error during Data Source initialization:", err)
    })

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/api', router);

app.listen(80, () => {
    console.log('Server is running at port 80');
});