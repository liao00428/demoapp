import { Request as req, Response as res } from "express";
import * as userService from '../services/user.service';

export const getUser = async (req: req, res: res) => {
    const result = await userService.getUser();
    res.json(result);
};

export const getOneUser = async (req: req, res: res) => {
    const userId = req.params.userId;
    const result = await userService.getOneUser(userId);
    res.json(result);
};

export const createUser = async (req: req, res: res) => {
    const { user } = req.body;
    const result = await userService.createUser(user);
    res.json(result);
};

export const deleteUser = async (req: req, res: res) => {
    const userId = req.params.userId;
    const result = userService.deleteUser(userId);
    res.json(result);
};