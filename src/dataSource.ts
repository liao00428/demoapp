// dataSource.ts
import { DataSource } from "typeorm"
import dotenv from 'dotenv';
dotenv.config();

export const dataSource = new DataSource({
    type: "postgres",
    host: process.env.DB_HOST,
    port: process.env.DB_PORT as unknown as number,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    entities: ["src/entities/*{.js,.ts}"],
    migrations: ["src/migrations/*{.js,.ts}"],
    logging: false,
    synchronize: false,
    migrationsRun: false,
    ssl: true,
    extra: {
        ssl: {
            rejectUnauthorized: false
        }
    }
})