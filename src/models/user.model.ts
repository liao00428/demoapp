import { dataSource } from "../dataSource";
import { User } from "../entities/user.entity";

export const getUser = async () => {
    return await dataSource.getRepository(User).find();
};

export const getOneUser = async (userId: string) => {
    return await dataSource.getRepository(User).findOneBy({ 
        id: userId
    });
};

export const createUser = (user: User) => {
    return dataSource.getRepository('User').save(user);
};

export const deleteUser = async (userId: string) => {
    await dataSource.getRepository(User).delete(userId);
};