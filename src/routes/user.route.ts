import { Router } from "express";
import * as userController from '../controllers/user.controller';

const userRouter = Router();

userRouter.get('/user', userController.getUser);
userRouter.get('/user/:userId', userController.getOneUser);
userRouter.post('/user', userController.createUser);
userRouter.delete('/user/:userId', userController.deleteUser);

export default userRouter;