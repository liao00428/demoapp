import { User } from '../entities/user.entity';
import * as userModel from '../models/user.model';

export const getUser = async () => {
    const result = await userModel.getUser();
    return result;
};

export const getOneUser = async (userId: string) => {
    const result = await userModel.getOneUser(userId);
    if (result) {
        result.name = 'Mr. ' + result.name;
    }
    return result;
};

export const createUser = async (user: any) => {
    const { name, age } = user;
    const newUser = new User();
    newUser.name = name;
    newUser.age = age;
    const result = await userModel.createUser(newUser);
    return result;
};

export const deleteUser = async (userId: string) => {
    await userModel.deleteUser(userId);
};